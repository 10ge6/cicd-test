ARG BASE_IMAGE=node:16.15-alpine

# =====================================
# build stage
# =====================================

FROM $BASE_IMAGE as builder

ENV NODE_ENV=test
ENV NEXT_TELEMETRY_DISABLED 1

RUN apk add --no-cache bash git curl
RUN curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm

WORKDIR /atvInfra
COPY package*.json pnpm-lock.yaml ./

RUN pnpm install --frozen-lockfile

COPY . ./

RUN NODE_ENV=production pnpm run build

# =====================================
# deploy stage
# =====================================

FROM $BASE_IMAGE

WORKDIR /atvInfra
COPY --from=builder /atvInfra ./

ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1

EXPOSE 3000/tcp

CMD [ "pnpm", "start"]
